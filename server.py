#! /usr/bin/python
# File:    server.py

from concurrent import futures
import logging

import grpc

import student_pb2
import student_pb2_grpc

STUDENTS = {
    17020095: {
        "ho_va_ten": "Mai Duy Duong",
        "ngay_sinh": "1999-09-13",
        "ma_sv": 17020095,
        "gioi_tinh": "Nam",
        "email_vnu": "17020095@vnu.edu.vn",
        "email_khac": "",
    }
}


def find_student(student_id):
    return student

class Student(student_pb2_grpc.StudentServicer):

    def Get(self, request, context):
        student = STUDENTS[request.number]
        return student_pb2.StudentInfo(
            name = student["ho_va_ten"],
            date_of_birth = student["ngay_sinh"],
            student_id = student["ma_sv"],
            sex = student["gioi_tinh"],
            email_vnu = student["email_vnu"],
            email_other = student["email_khac"]
        )

    
def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    student_pb2_grpc.add_StudentServicer_to_server(Student(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    server.wait_for_termination()
    

if __name__ == '__main__':
    logging.basicConfig()
    serve()
