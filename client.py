#! /usr/bin/python
# File:    client.py

from __future__ import print_function
import logging

import grpc

import student_pb2
import student_pb2_grpc


def run(id):
    with grpc.insecure_channel('localhost:50051') as channel:
        stub = student_pb2_grpc.StudentStub(channel)
        res = stub.Get(student_pb2.StudentId(number=id))
    return res
    
import tkinter as tk

def get_student_info():
    student_id = text_input.get("1.0", "end").strip()
    data = run(int(student_id))
    if data:
        body = ""
        body = body + "ho va ten: "+ data.name + "\n"
        body = body + "ma sinh vien: " + str(data.student_id) + "\n"
        body = body + "ngay sinh: " + data.date_of_birth + "\n"
        body = body + "gioi tinh: " + data.sex + "\n"
        body = body + "email vnu: " + data.email_vnu + "\n"
        body = body + "email khac: " + data.email_other + "\n"
        result.config(text=body)
    else:
        result.config(text="khong tim thay sinh vien voi ma so %s" % student_id)
        
window=tk.Tk()
window.title("Lay thong tin sinh vien")
window.geometry("600x400")

label = tk.Label(window, text = "Nhap ma so sinh vien:")
text_input = tk.Text(window, height = 1, width = 8)
button = tk.Button(window,
                   text = "Lay thong tin sinh vien",
                   command=get_student_info)
result = tk.Label(window, text = "...")

label.pack()
text_input.pack()
button.pack()
result.pack()

if __name__ == '__main__':
    logging.basicConfig()
    window.mainloop()
